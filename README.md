Este proyecto ejemplifica la integración de la aplicación BIRT Report Viewer (4.2.0) con una aplicación web existente basada en Maven.

Para mayor información consulte: https://www.ibm.com/developerworks/library/ba-birt-viewer-java-webapps/
